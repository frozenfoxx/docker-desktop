# docker-desktop
A Docker container that runs a Linux desktop.

Docker Hub: [https://hub.docker.com/r/frozenfoxx/desktop/](https://hub.docker.com/r/frozenfoxx/desktop/)

# How to Build

```
git clone git@gitlab.com:frozenfoxx/docker-desktop.git
cd docker-desktop
docker build -t frozenfoxx/desktop:latest .
```

# How to Use this Image
## Quickstart

The following will run the latest desktop.

```
docker run -d --rm -p 8080:8080 --name=desktop frozenfoxx/desktop:latest
```
